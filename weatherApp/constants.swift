//
//  constants.swift
//  weatherApp
//
//  Created by Raunak Singh on 31/8/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import Foundation

let baseURL = "http://api.openweathermap.org/data/2.5/"
let currentApi = "weather?"
let forcastApi = "forecast/daily?"
let count = "&cnt=11"
let lattitude = "lat="
let longitude = "&lon="
let appID = "&appid="
let apiKey = "792fd0d21f80ca1683a5c4ce5bac38b7"

typealias DownloadComplete = () -> ()

let CURRENT_WEATHER_URL = "\(baseURL)\(currentApi)\(lattitude)\(Location.sharedInstance.currentLatitude!)\(longitude)\(Location.sharedInstance.currentLongitude!)\(appID)\(apiKey)"
let FORECAST_URL = "\(baseURL)\(forcastApi)\(lattitude)\(Location.sharedInstance.currentLatitude!)\(longitude)\(Location.sharedInstance.currentLongitude!)\(count)\(appID)\(apiKey)"

