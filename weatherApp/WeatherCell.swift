//
//  WeatherCell.swift
//  weatherApp
//
//  Created by Raunak Singh on 7/9/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import UIKit

class WeatherCell: UITableViewCell {

    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var dayLabel: UILabel!
   
    @IBOutlet weak var weatherTypeLabel: UILabel!

    @IBOutlet weak var maxTempLabel: UILabel!
    
    @IBOutlet weak var minTempLabel: UILabel!
    
    func updateCells( forecast: Forecast) {
        dayLabel.text = forecast.date
        weatherTypeLabel.text = forecast.weatherType
        maxTempLabel.text = "\(Int(forecast.highTemp))"
        minTempLabel.text = "\(Int(forecast.lowTemp))"
        weatherIcon.image = UIImage(named: "\(forecast.weatherType) Mini")
    }
    
}
