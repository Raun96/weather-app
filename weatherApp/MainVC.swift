//
//  MainVC.swift
//  weatherApp
//
//  Created by Raunak Singh on 23/8/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var currentWeatherLabel: UILabel!
    @IBOutlet weak var currentWeatherImage: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var currentLocationLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var forecastArray = [Forecast]()
    var current_Weather = CurrentWeather()
    var forecast: Forecast!
    
    let locationManager = CLLocationManager()
    var currentLocation : CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startMonitoringSignificantLocationChanges()
        
        tableView.dataSource = self
        tableView.delegate = self
        current_Weather = CurrentWeather()
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        locationAuthStatus()
    }
    
    func locationAuthStatus() {
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            currentLocation = locationManager.location
          Location.sharedInstance.currentLatitude = currentLocation.coordinate.latitude
          Location.sharedInstance.currentLongitude = currentLocation.coordinate.longitude
            print("The lat and long of the location is \(Location.sharedInstance.currentLatitude!) & \(Location.sharedInstance.currentLongitude!)")
            // setup UI to load downloaded data.
            current_Weather.downloadWeatherData{
                self.downloadForecastData {
                    self.updateWeatherUI()
                }
            }
        } else {
            self.locationManager.requestWhenInUseAuthorization()
            locationAuthStatus()
        }
    }
    


    func downloadForecastData(completed: @escaping DownloadComplete){
        let forecastURL = URL(string: FORECAST_URL)!
        print(forecastURL)
        Alamofire.request(forecastURL) .responseJSON { response in
            let result = response.result
            if let dict = result.value as? Dictionary<String, AnyObject> {
                
                if let list = dict["list"] as? [Dictionary<String, AnyObject>] {
                    
                    for obj in list {
                        let forecast = Forecast(weatherDict: obj)
                        self.forecastArray.append(forecast)
                    }
                    self.forecastArray.remove(at: 0)
                    self.tableView.reloadData()
                }
            }
            
        }
        completed()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.forecastArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "weatherCell", for: indexPath) as? WeatherCell {
            let forecast = forecastArray[indexPath.row]
            cell.updateCells(forecast: forecast)
            return cell
        } else {
            return WeatherCell()
        }
    }
    
    func updateWeatherUI() {
        dateLabel.text = current_Weather.date
        currentWeatherLabel.text = current_Weather.weatherType
        currentLocationLabel.text = current_Weather.cityName
        temperatureLabel.text = "\(current_Weather.currentTemp)"
        currentWeatherImage.image = UIImage(named: current_Weather.weatherType)
    }

}

