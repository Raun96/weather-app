//
//  currentWeather.swift
//  weatherApp
//
//  Created by Raunak Singh on 31/8/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import UIKit
import Alamofire

class CurrentWeather{
    
    var _cityName: String!
    var _date: String!
    var _weatherType: String!
    var _currentTemp: Int!
    
    var cityName: String{
        if _cityName == nil{
            _cityName = ""
        }
        return _cityName
    }
    var currentTemp : Int{
        if _currentTemp == nil{
            _currentTemp = 0
        }
        return _currentTemp
    }
    var weatherType: String{
        if _weatherType == nil{
            _weatherType = ""
        }
        return _weatherType
    }
    
    var date: String{
        if _date == nil{
            _date = ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        let currentDate = dateFormatter.string(from: Date())
        self._date = "Today, \(currentDate)"
        return _date
    }
    
    func downloadWeatherData(completed: @escaping DownloadComplete){
        //Alamofire Download
        
        let currentWeatherURL = URL(string: CURRENT_WEATHER_URL)!
        Alamofire.request(currentWeatherURL).responseJSON { response in
            let result = response.result
            if let dict = result.value as? Dictionary<String,AnyObject> {
                
                if let name = dict["name"] as? String {
                    self._cityName = name.capitalized
                    print(self.cityName)
                }
                if let weather = dict["weather"] as? [Dictionary<String,AnyObject>] {
                    if let main = weather[0]["main"] as? String{
                        self._weatherType = main.capitalized
                        print(self.weatherType)
                    }
                }
                if let main = dict["main"] as? Dictionary<String,AnyObject> {
                    if let kelvinTemp = main["temp"] as? Double {
                        let celsiusTemp: Int = Int(kelvinTemp - 273.15)
                        self._currentTemp = celsiusTemp
                        print(self.currentTemp)
                    }
                }
            
            }
            completed()
        
        }
        
    }
    
}
