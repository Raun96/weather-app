//
//  Location.swift
//  weatherApp
//
//  Created by Raunak Singh on 8/9/17.
//  Copyright © 2017 Raunak Singh. All rights reserved.
//

import CoreLocation

class Location {
    static var sharedInstance = Location()
    private init() {}
    
    var currentLatitude: Double!
    var currentLongitude: Double!
}

